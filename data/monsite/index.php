<?php
require __DIR__ . '/src/MinecraftPing.php';
require __DIR__ . '/src/MinecraftPingException.php';

use xPaw\MinecraftPing;
use xPaw\MinecraftPingException;

try {
    $Query = new MinecraftPing('yourdomain', 25565);
    $result = $Query->Query();

    echo '<html>';
    echo '<head><title>Minecraft Server Information</title></head>';
    echo '<body>';

    if ($result !== false) {
        echo '<h1>Server Information</h1>';
        echo '<ul>';
        echo '<li><strong>Version:</strong> ' . $result['version']['name'] . ' (Protocol: ' . $result['version']['protocol'] . ')</li>';
        echo '<li><strong>Description:</strong> ' . $result['description'] . '</li>';
        
        $players = $result['players'];
        echo '<li><strong>Players Online:</strong> ' . $players['online'] . '/' . $players['max'] . '</li>';
        echo '</ul>';
    } else {
        echo '<p>Failed to retrieve server information.</p>';
    }

    echo '</body>';
    echo '</html>';
} catch (MinecraftPingException $e) {
    echo 'Exception: ' . $e->getMessage();
} finally {
    if ($Query) {
        $Query->Close();
    }
}
?>

