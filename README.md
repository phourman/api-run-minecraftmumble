# API-Run MinecraftMumble

This project use a singular docker-compose file to launch a minecaft 1.18.1 server, a mumble server, and a php website returning info about the minecraft server.

## Getting started

You have to have docker / docker-compose installed
You have to check that you don't have processes running using the ports 80, 9000, 64738, 443, and 25565

```
sudo apt-get install docker
sudo apt-get install docker-compose
```

Replace `yourdomain` by your server address in the whole project

Get you certificates (skip if you already have a certificate)

```
sudo apt-get install certbot
sudo certbot certonly --standalone -d your.domain.com
```

Copy the certificate in two locations (execute in project folder)

```
sudo cp -r /etc/letsencrypt .docker/conf/nginx
sudo chmod -R +rwx .docker/conf/nginx/letsencrypt
sudo mkdir mumble-certs
sudo cp .docker/conf/nginx/letsencrypt/live/yourdomain/privkey.pem mumble-certs/privkey.pem
sudo cp .docker/conf/nginx/letsencrypt/live/yourdomain/fullchain.pem mumble-certs/fullchain.pem
```


## Start servers

```
sudo docker-compose up -d           #Buil and start the containers
```

Other useful commands :
```
sudo docker-compose stop            #Stop the containers
sudo docker-compose restart         #Restart the containers
sudo docker-compose kill            #Kill the containers (rough)
sudo docker-compose down -v         #Destroy the containers (and volumes)
```


## Connect to the services

### Minecraft (1.18.1)

Connect to `yourdomain:25565`

### Mumble

Connect to `yourdomain:64738`

### PHP

Connect to `https://yourdomain`


## Minecraft server management

Run this command to get into the minecraft server console

```
sudo docker exec -it minecraft rcon-cli
```

Run this command to get into the bash prompt of the minecraft container

```
sudo docker exec -it minecraft /bin/bash
```


## Mumble server management

Run this command to get into the bash prompt of the mumble-server container

```
sudo docker exec -it mumble-server /bin/bash
```

## Saving services

### Mumble

No need for saving

### Minecraft

Data mounted on volume, if you want to create a save you can copy the `minecraft` folder

### PHP

No need for saving

## Add MumbleLink to Minecraft

MumbleLink is a mod allowing Mumble to get the players' positions in order to determine volume intensity

https://maven.minecraftforge.net/net/minecraftforge/forge/1.18.1-39.1.0/forge-1.18.1-39.1.0-installer.jar

https://www.curseforge.com/api/v1/mods/222432/files/4363341/download

On your computer :
- Start Minecraft 1.18.1 (so it downloads the 1.18.1 version on your computer)
- Stop Minecraft 
- Start `forge-1.18.1-39.1.0-installer.jar`
- Add `mumblelink-1.18.1-6.0.0.jar` to your local `/minecraft/mod` folder
- Configure Mumble https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/1272675-1-17-1-mumblelink-forge-smp-lan-mumble-ts3
- Restart Mumble and log in to the Mumble server `yourdomain:64738`
- Start Minecraft with the profile `Forge 1.18.1`
- Log in to the Minecraft Server `yourdomain:25565`
- Enjoy